# Progetto di laboratorio

## Linee guida
1. Visto che il progetto si compone di due parti separate, ciascuna avrà la sua subdirectory da usare per il codice e la relativa relazione
2. Il linguaggio utilizzato per il codice è `python`
3. Per costruire la relazione, usare 
   1. Google Docs, per proporre una bozza
   2. LaTeX per costruire la relazione
4. Nella relazione il prof non vuole codice, solo grafici e commenti

### Struttura di un subproject
- Il codice andrà in relativi moduli
- Il main sarà in un file separato, che eseguirà benchmark e possibilmente output dei grafici
  - TODO: Trovare libreria per benchmark simile a [criterion](http://www.serpentine.com/criterion/)

## Consegna
Oltre a consegnare la relazione bisogna verificare che il codice superi tutti i test case
Il documento verrà aggiornato con le modalità di consegna del codice

