#!/usr/bin/env python

class Node:
    def __init__(self, key: int, value: str):
        self.key = key
        self.value = value
