#!/usr/bin/env python

from typing import Optional

class AvlNode:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.left = None 
        self.right = None 

class AvlTree:
    def __init__(self):
        self.node = None 
        self.height = -1  
        self.balance = 0
                
    def height(self):
        if self.node is not None: 
            return self.node.height 
        else: 
            return 0 
    
    def is_leaf(self):
        return self.height == 0
    
    def insert(self, key, value):
        tree = self.node
        
        newnode = AvlNode(key, value)
        
        if tree is None:
            self.node = newnode 
            self.node.left = AvlTree() 
            self.node.right = AvlTree()
        
        elif key < tree.key: 
            self.node.left.insert(key, value)
            
        elif key > tree.key: 
            self.node.right.insert(key, value)
            
        self.rebalance() 
        
    def rebalance(self):
        self.update_heights(False)
        self.update_balances(False)
        while self.balance < -1 or self.balance > 1: 
            if self.balance > 1:
                if self.node.left.balance < 0:  
                    self.node.left.lrotate() # Case 2
                    self.update_heights()
                    self.update_balances()
                self.rrotate()
                self.update_heights()
                self.update_balances()
                
            if self.balance < -1:
                if self.node.right.balance > 0:  
                    self.node.right.rrotate() # Case 3
                    self.update_heights()
                    self.update_balances()
                self.lrotate()
                self.update_heights()
                self.update_balances()
 
    def rrotate(self):
        # Rotate left pivoting on self
        A = self.node 
        B = self.node.left.node 
        T = B.right.node 
        
        self.node = B 
        B.right.node = A 
        A.left.node = T 

    
    def lrotate(self):
        # Rotate left pivoting on self
        A = self.node 
        B = self.node.right.node 
        T = B.left.node 
        
        self.node = B 
        B.left.node = A 
        A.right.node = T 

    def update_heights(self, rec=True):
        if self.node is not None: 
            if rec: 
                if self.node.left is not None: 
                    self.node.left.update_heights()
                if self.node.right is not None:
                    self.node.right.update_heights()
            
            self.height = max(self.node.left.height,
                              self.node.right.height) + 1 
        else: 
            self.height = -1 

    def update_balances(self, rec=True):
        if self.node is not None: 
            if rec: 
                if self.node.left is not None: 
                    self.node.left.update_balances()
                if self.node.right is not None:
                    self.node.right.update_balances()

            self.balance = self.node.left.height - self.node.right.height 
        else: 
            self.balance = 0 

    def delete(self, key):
        if self.node is None:
            return
        
        if self.node.key == key: 
            if self.node.left.node is None and self.node.right.node == None:
                self.node = None
            # if only one subtree, take that 
            elif self.node.left.node == None: 
                self.node = self.node.right.node
            elif self.node.right.node == None: 
                self.node = self.node.left.node
            
            # Has both children
            else:  
                succ = self.logical_successor()
                self.node.key = succ.key 
                    
                self.node.right.delete(succ.key)
                
            self.rebalance()
            return  
        elif key < self.node.key: 
            self.node.left.delete(key)  
        elif key > self.node.key: 
            self.node.right.delete(key)
                    
        self.rebalance()

    def logical_successor(self):
        return self.node.right.min()

    def logical_predecessor(self):
        return self.node.left.max()

    def min(self):
        if self.node.left is None:
            return self.node
        else:
            return self.node.left.min()

    def max(self):
        if self.node.right is None:
            return self.node
        else:
            return self.node.right.max()


    def check_balanced(self):
        if self.node is None: 
            return True
        
        self.update_heights()
        self.update_balances()
        return abs(self.balance) < 2 and self.node.left.check_balanced() and self.node.right.check_balanced()
        
    def inorder(self):
        if self.node is None:
            return []
        return self.node.left.inorder() + [self.node.key] + self.node.right.inorder()

    def clear(self):
        self.node = None

    def search(self, key):
        if self.node is None:
            return None
        if self.node.key == key:
            return self.node
        elif self.node.key > key:
            return self.node.left.search(key)
        else:
            return self.node.right.search(key)

    def __str__(self):
        if self.node is None:
            return "NULL"
        else:
            return str(self.node.key) + ':' + self.node.value + ':' + str(self.height + 1) + ' ' + str(self.node.left) + ' ' + str(self.node.right)


if __name__ == "__main__":
    from sys import stdin

    avl = AvlTree()
    while True:
        cmd = stdin.readline()
        words = [s.strip() for s in cmd.split(' ')]
        command = words[0]

        if command == "clear":
            avl.clear()
        elif command == "show":
            print(str(avl))
        elif command == "insert":
            key = int(words[1])
            value = words[2]
            avl.insert(key, value)
        elif command == "find":
            key = int(words[1])
            node = avl.search(key)
            if node is not None:
                print(node.value)
        else:
            break
