#/usr/bin/env python

## Used when deletion is enabled to print errors
# import sys

class RedBlackNode:
    def __init__(self, key: int, value: str):
        self.is_black: bool = False
        self.parent: 'RedBlackNode' = None
        self.left: 'RedBlackNode' = None
        self.right: 'RedBlackNode' = None
        self.key: int = key
        self.value: str = value

    def color(self):
        if self.is_black:
            return "Black"
        else:
            return "Red"
        
    def __str__(self):
        return "{}:{}:{} {} {}".format(self.key,
                                           self.value,
                                           self.color(),
                                           self.left if self.left is not None else "NULL",
                                           self.right if self.right is not None else "NULL")

    def uncle(self):
        if self.parent and self.parent.parent:
            if self.parent.parent.left is self.parent:
                return self.parent.parent.right
            else:
                return self.parent.parent.left
        else:
            return None

    def sibling(self):
        if self.parent:
            if self.parent.left is self:
                return self.parent.right
            else:
                return self.parent.left
        else:
            return None

    def min_descendent(self):
        if not self.left:
            return self
        else:
            return self.left.min_descendent()
        
    # self must have a right child
    def successor(self):
        return self.right.min_descendent()
        

    def children_num(self):
        return int(bool(self.left)) + int(bool(self.right))

    def only_child(self):
        return self.left or self.right

    def setleft(self, n: 'RedBlackNode'):
        self.left = n
        if n is not None:
            n.parent = self

        
    def setright(self, n: 'RedBlackNode'):
        self.right = n
        if n is not None:
            n.parent = self

    #    A
    #   / \
    #  B   C
    #     / \
    #    D   E
    def left_rotate(self):
        parent = self.parent
        
        c = self.right
        d = self.right.left

        self.setright(d)

        c.setleft(self)

        if parent is not None:
            if parent.left is self:
                parent.setleft(c)
            else:
                parent.setright(c)
        else:
            c.parent = None

        return c

    def right_rotate(self):
        parent = self.parent
        
        c = self.left
        d = self.left.right

        self.setleft(d)

        c.setright(self)

        if parent is not None:
            if parent.left is self:
                parent.setleft(c)
            else:
                parent.setright(c)
        else:
            c.parent = None
            
        return c

    def is_straight_left(self, node):
        if self.left and self.left.left is node:
            return True
        else:
            return False

    def is_straight_right(self, node):
        if self.right and self.right.right is node:
            return True
        else:
            return False

    def is_left_right_angle(self, node):
        if self.left and self.left.right is node:
            return True
        else:
            return False

    def is_right_left_angle(self, node):
        if self.right and self.right.left is node:
            return True
        else:
            return False
        
    def is_straight(self, node):
        if self.is_straight_left(node) or self.is_straight_right(node):
            return True
        else:
            return False

    def __search__(self, key):
        if self.key == key:
            return self
        elif self.key > key:
            if self.left is None:
                return None
            else:
                return self.left.__search__(key)
        else:
            if self.right is None:
                return None
            else:
                return self.right.__search__(key)

    def __insert__(self, node: 'RedBlackNode'):
        if self.key > node.key:
            if self.left is None:
                self.setleft(node)
            else:
                self.left.__insert__(node)
        else:
            if self.right is None:
                self.setright(node)
            else:
                self.right.__insert__(node)

def __is_node_black__(node):
    if node is not None:
        return node.is_black
    else:
        return True
    
class RedBlackTree:
    def __init__(self):
        self.root = None

    def __str__(self):
        if self.root is None:
            return "NULL"
        else:
            return str(self.root)

    def search(self, key):
        if self.root is None:
            return None
        return self.root.__search__(key)

    def clear(self):
        self.root = None

    def insert(self, key, value):
        node = RedBlackNode(key, value)
        if self.root is None:
            self.root = node
            self.__resolve_issues__(node)

        else:
            self.root.__insert__(node)
            self.__resolve_issues__(node)

    def __resolve_issues__(self, node):
        if node is self.root:
            node.is_black = True
            
        elif not node.parent.is_black:
            granddad = node.parent.parent
            
            if not __is_node_black__(node.uncle()):
                node.parent.is_black = True
                node.uncle().is_black = True
                granddad.is_black = False
                self.__resolve_issues__(granddad)

            elif granddad.is_straight_left(node):
                if self.root is granddad:
                    self.root = node.parent
                granddad.right_rotate()
                granddad.is_black = False
                node.parent.is_black = True

            elif granddad.is_straight_right(node):
                if self.root is granddad:
                    self.root = node.parent
                granddad.left_rotate()
                granddad.is_black = False
                node.parent.is_black = True

            elif granddad.is_left_right_angle(node):
                node.parent.left_rotate()
                self.__resolve_issues__(node.left)
                
            elif granddad.is_right_left_angle(node):
                node.parent.right_rotate()
                self.__resolve_issues__(node.right)


if __name__ == "__main__":
    from sys import stdin

    bst = RedBlackTree()
    while True:
        cmd = stdin.readline()
        words = [s.strip() for s in cmd.split(' ')]
        command = words[0]

        if command == "clear":
            bst.clear()
        elif command == "show":
            print(str(bst))
        elif command == "insert":
            key = int(words[1])
            value = words[2]
            bst.insert(key, value)
        elif command == "find":
            key = int(words[1])
            node = bst.search(key)
            if node is not None:
                print(node.value)
        else:
            break
        
