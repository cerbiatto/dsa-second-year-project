#!/usr/bin/env python

import typing

class BinarySearchNode:
    def __init__(self, key: int, value: str, parent: typing.Optional['BinarySearchNode'] = None):
        self.key = key
        self.value = value
        self.parent: typing.Optional['BinarySearchNode'] = parent
        self.left:   typing.Optional['BinarySearchNode'] = None
        self.right:  typing.Optional['BinarySearchNode'] = None


    def insert_node(self, node):
        if node.key < self.key:
            if self.left is None:
                self.left = node
                node.parent = self
            else:
                self.left.insert_node(node)
        else:
            if self.right is None:
                self.right = node
                node.parent = self
            else:
                self.right.insert_node(node)
                
    def insert(self, key: int, value: str):
        "Insert a new key/value pair in the tree"
        self.insert_node(BinarySearchNode(key, value))

    def search(self, key: int) -> typing.Optional['BinarySearchNode']:
        if self.key == key:
            return self
        if self.key < key:
            return self.right.search(key)\
                   if self.right is not None else\
                   None
        else:
            return self.left.search(key)\
                   if self.left is not None else\
                   None
    
    def __str__(self):
        return f'{self.key}:{self.value} {str(self.left) if self.left is not None else "NULL"} {str(self.right) if self.right is not None else "NULL"}'

class BinarySearchTree:
    def __init__(self):
        self.root: typing.Optional['BinarySearchNode']  = None


    def insert(self, key: int, value: str):
        node = BinarySearchNode(key, value)
        if self.root is None:
            self.root = node
        else:
            self.root.insert_node(node)

    def search(self, key: int):
        if self.root is None:
            return None

        return self.root.search(key)

    def clear(self):
        self.root = None

    def __str__(self) -> str:
        if self.root is not None:
            return str(self.root)
        else:
            return "NULL"

if __name__ == "__main__":
    from sys import stdin

    bst = BinarySearchTree()
    while True:
        cmd = stdin.readline()
        words = [s.strip() for s in cmd.split(' ')]
        command = words[0]

        if command == "clear":
            bst.clear()
        elif command == "show":
            print(str(bst))
        elif command == "insert":
            key = int(words[1])
            value = words[2]
            bst.insert(key, value)
        elif command == "find":
            key = int(words[1])
            node = bst.search(key)
            if node is not None:
                print(node.value)
        else:
            break
        
