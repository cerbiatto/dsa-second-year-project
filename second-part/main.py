#!/usr/bin/env python

from tools.benchmark import Benchmark
from csv import writer
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def write_to_csv(benchmark, file):
    bst_means = [Benchmark.mean(times) for times in benchmark.bst_times]
    avl_means = [Benchmark.mean(times) for times in benchmark.avl_times]
    rbt_means = [Benchmark.mean(times) for times in benchmark.rbt_times]

    bst_stddev = [Benchmark.stddev(times) for times in benchmark.bst_times]
    avl_stddev = [Benchmark.stddev(times) for times in benchmark.avl_times]
    rbt_stddev = [Benchmark.stddev(times) for times in benchmark.rbt_times]

    w = writer(file)

    w.writerow(['point', 'time', 'dev', 'tree'])
    for p,m,s in zip(benchmark.points, bst_means, bst_stddev):
        w.writerow([p, m, s, 'BST'])
    for p,m,s in zip(benchmark.points, avl_means, avl_stddev):
        w.writerow([p, m, s, 'AVL'])
    for p,m,s in zip(benchmark.points, rbt_means, rbt_stddev):
        w.writerow([p, m, s, 'RBT'])

def compute_csv():
    data = pd.read_csv('csv/data.csv')
    
    sns.lineplot(x='point',y='time',hue='tree', data=data)
    plt.title('Execution Time - Linear')
    plt.xlabel('String length')
    plt.ylabel('Execution time')
    plt.savefig('out/time-linear.png')
    plt.clf()

    sns.lineplot(x='point',y='time',hue='tree', data=data)
    plt.title('Execution Time - Logarithmic')
    plt.xlabel('String length')
    plt.ylabel('Execution time')
    plt.xscale('log')
    plt.yscale('log')
    plt.savefig('out/time-log.png')
    plt.clf()

    sns.lineplot(x='point',y='dev',hue='tree', data=data)
    plt.title('Standard Deviation - Linear')
    plt.xlabel('String length')
    plt.ylabel('Standard deviation')
    plt.savefig('out/stddev-linear.png')
    plt.clf()

    sns.lineplot(x='point',y='dev',hue='tree', data=data)
    plt.title('Standard Deviation - Linear')
    plt.xlabel('String length')
    plt.ylabel('Standard deviation')
    plt.xscale('log')
    plt.yscale('log')
    plt.savefig('out/stddev-log.png')
    plt.clf()

def main():
    min  = 1000
    max  = 100000
    reps = 100
    # benchmark = Benchmark(min, max, reps=reps)

    # To generate the csv
    # with open('csv/data.csv', 'w') as f:
    #     write_to_csv(benchmark, f)

    ## To compute the generated csv
    compute_csv()
    
    # bst_means = [Benchmark.mean(times) for times in benchmark.bst_times]
    # avl_means = [Benchmark.mean(times) for times in benchmark.avl_times]
    # rbt_means = [Benchmark.mean(times) for times in benchmark.rbt_times]

    # bst_stddev = [Benchmark.stddev(times) for times in benchmark.bst_times]
    # avl_stddev = [Benchmark.stddev(times) for times in benchmark.avl_times]
    # rbt_stddev = [Benchmark.stddev(times) for times in benchmark.rbt_times]
    
    # times = zip(benchmark.points,
    #             bst_means,
    #             bst_stddev,
    #             avl_means,
    #             avl_stddev,
    #             rbt_means,
    #             rbt_stddev)

    # for time in times:
    #     print(' '.join(str(t) for t in time))
    

if __name__ == "__main__":
    main()
