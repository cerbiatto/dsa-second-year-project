import sys
from time import get_clock_info, time_ns
from random import randint, choices
from trees.bst import BinarySearchTree
from trees.avl import AvlTree
from trees.rbt import RedBlackTree
import math



def test_point(minV: int, maxV: int, n_points: int):
    """Generates lengths of strings, following a logarithmic growth"""
    base = math.exp((math.log(maxV) - math.log(minV)) / n_points)
    yield from (int(minV * math.pow(base, i)) for i in range(n_points))

class Benchmark:
    def __init__(self, min, max, n=100, reps = 1):

        sys.setrecursionlimit(max+10)
        self.min = min
        self.max = max
        self.points = tuple(test_point(min, max, n))

        resolution = get_clock_info('time').resolution
        max_relative_error = 0.01
        self.min_measurable_time = resolution / max_relative_error + resolution
        
        self.bst_times = [Benchmark.bench(BinarySearchTree, n) for _ in range(reps) for n in self.points]
        self.avl_times = [Benchmark.bench(AvlTree,          n) for _ in range(reps) for n in self.points]
        self.rbt_times = [Benchmark.bench(RedBlackTree,     n) for _ in range(reps) for n in self.points]


    @staticmethod
    def bench(tree_class, n):
        tree = tree_class()
        times = []
        points = choices(range(n), k=n)
        start = time_ns()
        for i in range(n):
            Benchmark.searchorinsert(tree, points[i])
            times.append(time_ns())
        for i in range(n - 1, 0, -1):
            times[i] -= times[i-1]
        if n > 0:
            times[0] -= start
        return times

    @staticmethod
    def searchorinsert(tree, number):
        node = tree.search(number)
        if node is None:
            tree.insert(number, str(number))

    @staticmethod
    def mean(times):
        return sum(times) / len(times)

    @staticmethod
    def stddev(times):
        mean = Benchmark.mean(times)
        variance = [(t - mean) ** 2 for t in times]
        return math.sqrt(sum(variance)/len(times))
