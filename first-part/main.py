#!/usr/bin/env python

import tableprint as tp
from csv import writer

from argparse import ArgumentParser

from tools.benchmark import Benchmark, BenchmarkType
from tools.string_generator import random_string, periodic_string, almost_periodic, period_naive_worst
from tools.compute import compute

def f5(time: float) -> str:
    return "{:.5e}".format(time)

def bench(f, min_length, max_length, points, gen, type, rep):
    benchmark = Benchmark(min_length, max_length, points, generator=gen,type=type, rep=rep)
    b = type == BenchmarkType.STANDARD_DEVIATION
    headers = ['# pt.', 'String Length', 'Naive Time', 'Smart Time', 'Naive - Smart'] + (['Naive Deviation', 'Smart Deviation'] if b else [])

    rows = zip(range(len(benchmark)),
               map(len, benchmark.static_strings),
               map(f5, benchmark.naive_times),
               map(f5, benchmark.smart_times),
               map(f5, benchmark.difference()),
               map(f5, benchmark.naive_sd),
               map(f5, benchmark.smart_sd)) \
           if b else\
           zip(range(len(benchmark)),
               map(len, benchmark.static_strings),
               map(f5, benchmark.naive_times),
               map(f5, benchmark.smart_times),
               map(f5, benchmark.difference()))
    rows = tuple(rows)

    w = writer(f)
    w.writerow(['id', 'length', 'time'] + (['stddev'] if b else []) + ['type'])
    for row in rows:
        w.writerow([row[0],       row[1], row[2]] + (row[5] if b else []) + ['naive'])
        w.writerow([row[0] + 100, row[1], row[3]] + (row[6] if b else []) + ['smart'])
    tp.table(rows, headers)


def benchmark():
    min_length = 1000       # Lunghezza minima stringa
    max_length = 500000     # Lunghezza massima stringa
    points = 99             # Numero di punti da generare
    reps = 1000
    
    print('1. Random strings:')
    with open('csv/random.csv', 'w') as f:
        bench(f, min_length, max_length, points, random_string, BenchmarkType.STANDARD_DEVIATION, reps)

    print('2. Periodic strings:')
    with open('csv/periodic.csv', 'w') as f:
        bench(f, min_length, max_length, points, periodic_string, BenchmarkType.STANDARD_DEVIATION, reps)

    print('3. Almost periodic strings:')
    with open('csv/almost-periodic.csv', 'w') as f:
        bench(f, min_length, max_length, points, almost_periodic, BenchmarkType.STANDARD_DEVIATION, reps)

    print('4. Period naive worst case:')
    with open('csv/naive-worst.csv', 'w') as f:
        bench(f, min_length, max_length, points, period_naive_worst, BenchmarkType.SIMPLE, reps)


def main():
    parser = ArgumentParser(description='Algorithm tester')
    parser.add_argument('op', help='test/compute')
    args = parser.parse_args()
    if args.op == 'test':
        benchmark()
    elif args.op == 'compute':
        compute()
    
if __name__ == '__main__':
    main()
