#!/usr/bin/env python3
import numpy as np
from time import get_clock_info, time_ns
from enum import Enum, auto
from typing import Callable, Iterator

from period.period_naive import period_naive
from period.period_smart import period_smart
from tools.string_generator import test_point, random_string, periodic_string, almost_periodic, period_naive_worst

class BenchmarkType(Enum):
    SIMPLE = auto()
    STANDARD_DEVIATION = auto()

class Benchmark:
    """Provides benchmarking results and tools"""
    
    def __init__(
            self, 
            min_length: int, 
            max_length: int, 
            points: int, 
            generator: Callable = random_string, 
            type: BenchmarkType = BenchmarkType.SIMPLE,
            rep: int = 100):
        """Initialize and run a benchmark"""

        lengths = list(test_point(min_length, max_length, points))
        
        resolution = get_clock_info('time').resolution
        max_relative_error = 0.001
        self.min_measurable_time = resolution / max_relative_error + resolution

        self.static_strings = tuple(generator(l) for l in lengths)

        if type == BenchmarkType.SIMPLE:
            self.naive_times = list(self.benchmark(period_naive))
            self.smart_times = list(self.benchmark(period_smart))

        elif type == BenchmarkType.STANDARD_DEVIATION:
            self.naive_times, self.naive_sd = self.benchmark_std(period_naive, rep)
            self.smart_times, self.smart_sd = self.benchmark_std(period_smart, rep)


    def difference(self) -> tuple:
        """Calculates differences between naive and smart runs"""
        
        return tuple(n - s for n, s in zip(self.naive_times, self.smart_times))

    def benchmark_std(
            self, 
            function: Callable[[str], int], 
            rep: int    
        ) -> tuple:
        """Benchmarks a function over set strings.
        Each test is repeated rep times, and a standard deviation and mean
        for each is returned."""

        means = []
        standard_deviations = []
        results = (self.standard_deviation(function, s, rep) for s in self.static_strings)
        for mean, std in results:
            means.append(mean)
            standard_deviations.append(std)
        return means, standard_deviations


    def standard_deviation(
            self, 
            function: Callable[[str], int], 
            string: str,
            rep: int       
        ) -> tuple:
        """Benchmarks function over a string n times, returns mean time and 
        standard deviation""" 

        res = tuple(self.benchmark_times(function, string) for _ in range(rep))
        mean = float(np.mean(res))
        std = float(np.std(res))
        return mean, std

    def benchmark(self, fun: Callable[[str], int]) -> Iterator[float]:
        """Benchmarks a function over set strings"""
        
        yield from (self.benchmark_times(fun, s) for s in self.static_strings)


    def benchmark_times(self, fun: Callable[[str], int], string: str) -> float:
        """Benchmarks a function over the provided string, 
        repeating the benchmark as long as it's needed 
        to provide a meaningful measurement"""

        count = 0
        start = time_ns()
        while True:
            fun(string)
            end = time_ns()
            count += 1
            if end - start > self.min_measurable_time:
                break
        return (end - start) / count
            

    def __len__(self):
        """Length is equivalent to number of run tests"""

        return len(self.static_strings)
