from csv import reader
from numpy import transpose
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

def compute_csv(f: str, log=False, outputdir = 'out', inputdir = 'csv'):
    data = pd.read_csv(f'{inputdir}{f}')
    title_base = f[:-4].replace('-', ' ').capitalize()
    filename = f[:-4]
    sns.lineplot(x='length',y='time',hue='type', data=data)
    plt.title(title_base + ' string generation')
    plt.xlabel('String length')
    plt.ylabel('Execution time')
    if log:
        plt.xscale('log')
        plt.yscale('log')
        filename += '-log'
    plt.savefig(f'{outputdir}/{filename}.png')
    print(title_base)
    plt.clf()

    if 'stddev' not in data:
        return

    filename += '-stddev'
    sns.lineplot(x='length',y='stddev',hue='type', data=data)
    plt.title(title_base + ' standard deviation')
    plt.xlabel('String length')
    plt.ylabel('Standard deviation')
    if log:
        plt.xscale('log')
        plt.yscale('log')
    plt.savefig(f'{outputdir}/{filename}.png')
    print(title_base)
    plt.clf()
    
                 
 
def compute():
    compute_csv('random.csv')
    compute_csv('periodic.csv')
    compute_csv('almost-periodic.csv')
    compute_csv('naive-worst.csv')
    compute_csv('random.csv', log=True)
    compute_csv('periodic.csv', log=True)
    compute_csv('almost-periodic.csv', log=True)
    compute_csv('naive-worst.csv', log=True)
