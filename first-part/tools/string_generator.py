#!/usr/bin/env python3
"""Provides generation function for the benchmark data"""

from string import ascii_lowercase
import random
import math
from typing import Iterator


def test_point(minV: int, maxV: int, n_points: int) -> Iterator[int]:
    """Generates lengths of strings, following a logarithmic growth"""
    base = math.exp((math.log(maxV) - math.log(minV)) / n_points)
    yield from (int(minV * math.pow(base, i)) for i in range(n_points))

# 1. Random string 
def random_string(length):
    alphabet = ('a', 'b', 'c')
    str = ""

    while len(str) < length:
        str += alphabet[random.randint(0, len(alphabet) - 1)]

    return str

# 2. Makes a random periodic string
def periodic_string(length):
    alphabet = ('a', 'b', 'c')
    q = random.randint(1, length + 1)

    # Random part of length q
    gen_str = random.choices(alphabet, k = q)

    # Periodic part for the rest of the string
    gen_str = periodic(gen_str, length, q)
    
    return ''.join(gen_str)

# 3. Random periodic string, but one letter off
def almost_periodic(length):
    alphabet = {'a', 'b', 'c'}
    not_alpha = set(ascii_lowercase) - alphabet

    q = random.randint(1, length + 1)
    
    gen = random.choices(tuple(alphabet), k = q)
    gen = periodic(gen, length, q)
    gen[q - 1] = random.choice(tuple(not_alpha))

    return ''.join(gen)

# 4. Worst case for period_naive
def period_naive_worst(length):
    alphabet = list('abc')
    random.shuffle(alphabet)
    return alphabet[0] * (length - 1) + alphabet[1]


def periodic(gen: list, length: int, q: int):
    return gen * (length // q) +\
        gen[:length % q]


def main():
    pass


if __name__ == '__main__':
    main()
