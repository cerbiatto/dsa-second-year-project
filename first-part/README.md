# Prima parte

Questa directory è la root per la prima parte del progetto di laboratorio

## TL;DR del progetto:
Calcolare le richieste e fare una relazione in pdf con
- Discussione degli algoritmi
- Grafici, in scala lineare e logaritmica
### 1. Trovare la lunghezza del periodo di una stringa
Esempio:
| Input    | Periodo | Output |
|----------|---------|--------|
| abcabc   | abc     | 3      |
| abcabcab | abc     | 3      |
| aba      | ab      | 2      |
| abca     | abc     | 3      |
| abccb    | abccb   | 5      |

Come si vede negli esempi, l'ultima ripetizione del periodo non deve essere necessariamente completa

Se la stringa non è periodica il periodo sarà lungo quanto l'intera stringa

Vengono richieste due implementazioni, una easy e una studiata. Entrambe vengono descritte nel dettaglio dal prof quindi non c'è molto da pensare

#### 1 - PeriodNaive
La prima implementazione si basa sulla definizione della proprietà di periodicità di una stringa:

$`(\star) \qquad \forall i = 1,\dots,n-p\quad s(i) = s(i+p)`$

Pseudocode dell'algoritmo del prof:
```python
s = 'abcabcab'
n = len(s)
for p in range(1, n + 1): # 1..n
    # Metodo 1
	for i in range(n - p): # 0..n-p-1
	    if s[i] != s[i + p]:
		    pass # È la p sbagliata
	else: # se tutti gli if nel for hanno fallito -> la p funziona
	    return p
		
	# Metodo 2
	if all(s[i:i+p] == s[i+p:i+p+p] for i in range(n-p-p)): # Se vale la proprietà \star per ogni substring
	    return p
```

#### 2 - PeriodSmart
Questa implementazione si basa sui bordi della stringa

Ad esempio in `aaabbbbbbbbbaaa`, `aaa` è un bordo, perché è sia prefisso che suffisso della stringa

1. Chiamiamo `r(i)` la lunghezza del bordo massimo di `s[:i]`
2. `n` sarà `len(s)`
3. Una volta trovato `r(i)`, `p = n - r(p)`

Esempi
| Input    | Length | Border | Border Length | Period Length | Period | Output |
|----------|--------|--------|---------------|---------------|--------|--------|
| abcabc   | 6      | abc    | 3             | 3             | abc    | 3      |
| abcabcab | 8      | abcab  | 5             | 3             | abc    | 3      |
| aba      | 3      | a      | 1             | 2             | ab     | 2      |
| abca     | 4      | a      | 1             | 3             | abc    | 3      |
| abccb    | 5      |        | 0             | 5             | abccb  | 5      |

Quindi quello che bisogna fare è trovare `r(n)` per trovare `p`

##### Costruzione di `r`
`r` può essere implementato sia come una funzione (con caching) che come lista da usare in un metodo iterativo, la definizione verrà data come funzione

- **Nota:** un bordo non viene considerato tale se coincide con l'intera stringa.
  Ad esempio dato `s = "a"`, `"a"` non sarà bordo di `s` nonostante sia tecnicamente prefisso e suffisso della stringa
  
```
r(1) = 0 -- per la nota data poco fa

r(i) = r(i - 1) + 1 se s[i - 1] == s[r(i - 1) + 1]
     = r(r(i - 1)) + 1 se s[i - 1] == s[r(r(i-1)) + 1]
	 = ...
	 = r^j (i - 1) + 1 se s[i - 1] == s[r^j (i - 1) + 1]
	 = 0 altrimenti
```

Procedo ricorsivamente con la funzione finché non trovo un elemento il cui successivo è uguale al nuovo carattere (i-esimo), e setto r(i) come lui + 1.

Sono solo presenti i caratteri implementativi, se vuoi chiarimenti per capire concettualmente la funzione controlla le lezioni 5/6 di laboratorio o chiedi nel gruppo.
	
### Misurare il tempo di esecuzione
Un programma per misurare i tempi medi di esecuzione per una stringa lunga $`n`$, con errore massimo relativo E = 0.001
- $`1000 \le n \le 500000`$
- Distribuzione esponenziale
Per chiarimenti leggere [qui](#modalità-di-consegna)
- $`s`$ viene generata da un'alfabeto con 2 o 3 elementi in uno dei modi seguenti:
  1. Ogni lettera in modo casuale
  2. Generando casualmente [1..q] con $`q \in 1..n`$ e ripetendo la stringa generata fino a n
  ```haskell
  -- In haskell,
  randomPart alphabet q = map (getRandom alphabet) [1..q]
  
  myString n q alphabet = take n . repeat $ randomPart alphabet q
  ```
  3. Come 2. ma s[q] = una lettera non nell'alfabeto
  4. (opzionale) Caso deterministico in cui viene costruito il caso pessimo per `PeriodNaive`
  
  
#### Come misurare il tempo
1. Trovare la *risoluzione* del clock -> il minimo intervallo misurabile
```c
start = now();
do {
	end = now();
} while (start == end);
return end - start;
```
2. Calcolare il tempo minimo misurabile $`T_{min} = R \times \left( \frac 1 E + 1\right)`$ dove
   - R è la risoluzione
   - E è l'errore relativo massimo ammissibile (0.001)
3. Ciclare l'algoritmo finché il tempo trascorso < $`T_{min}`$
Otteniamo il tempo medio di esecuzione dell'algoritmo $`T_{mid} = \frac {T_{trascorso}} {n_{iterazioni}}`$
4. (opzionale) Stimare deviazione standard dei tempi di esecuzione
  
  
## Consegna

La prima parte del progetto richiede l'implementazione di due algoritmi per il calcolo del periodo frazionario minimo di una stringa e l'analisi dei loro tempi medi di esecuzione. Il periodo frazionario minimo di una stringa s è il più piccolo intero $`p>0`$ che soddisfa la proprietà seguente:

$`(\star) \qquad \forall i = 1,\dots,n-p\quad s(i) = s(i+p)`$

Dove $`n`$ denota la lunghezza della stringa $`s`$

Di seguito si riportano alcuni esempi di istanze di input (stringa $`s`$) con il corrispondente output desiderato (periodi frazionario minimo di $`s`$):
| Input    | Output |
|----------|--------|
| abcabcab | 3      |
| aba      | 2      |
| abca     | 3      |

I due algoritmi da implementare, denominati PeriodNaive e PeriodSmart, sono descritti di seguito

### PeriodNaive
L'algoritmo utilizza un ciclo *for* con un intero $`p`$ che varia da $`1`$ a $`n`$. Alla prima iterazione che soddisfa la proprietà $`(\star)`$ l'algoritmo termina restituendo $`p`$.
La verifica della proprietà $`(\star)`$ può essere implementata in uno dei due modi seguenti

1. utilizzando un ciclo secondario e controllando direttamente che $`s(i) = s(i+p)`$ per ogni $`i = 1,\dots,n-p`$
2. oppure utilizzando le funzioni *strcmp* e *strsub* del linguaggio C (o analoghe funzioni per gli altri linguaggi) per confrontare la congruenza fra il prefisso $`s[1,n-p]`$ e il prefisso $`s[p+1,n]`$.

Entrambe le implementazioni richiedono, nel caso pessimo, tempo quadratico nella lunghezza $`n`$ della stringa.

### PeriodSmart

Si consideri la seguente definizione: un bordo si una stringa $`s`$ è una qualunque stringa $`t`$ che sia, allo stesso tempo, prefisso proprio di $`s`$ e suffisso proprio di $`s`$. Ad esempio, $`t=abab`$ è un bordo di $`s=ababaabababab`$, ed è anche il bordo di $`s`$ di lunghezza massima.
Si osservi quindi che $`p`$ è un periodo frazionario di $`s`$ se e solo se $`p=\vert s\vert−r`$ ed $`r`$ è la lunghezza di un bordo di $`s`$. Ciò permette di ridurre il problema del calcolo del periodo frazionario minimo di $`s`$ al problema del calcolo della lunghezza massima di un bordo di $`s`$.
Per risolvere quest'ultimo problema si proceda per induzione, calcolando per ogni prefisso $`s[1\dots i]`$, dal più corto al più lungo, la lunghezza $`r(i)`$ del bordo massimo di $`s[1\dots i]`$. Per implementare il passo induttivo da $`i`$ ad $`i+1`$, si consideri la sequenza $`r(i) > r(r(i)) > r(r(r(i))) \dots`$ e si osservi che nel calcolo $`r(i+1)`$ solamente i due casi seguenti possono darsi:

1. Per qualche indice $`j \le k`$ vale l'uguaglianza $`s[i+1] = s[r^j(i) + 1]`$; in tal caso $`r(i+1) = r^j(i)+1`$, dove $`j`$ è il primo indice per cui vale la suddetta uguaglianza;
2. Non esiste alcun indice $`j \le k`$ che soddisfi l'uguaglianza $`s[i+1] = s[r^j(i) + 1]`$; in tal caso $`r(i+1)=0`$

Per calcolare la lunghezza massima di un bordo di $`s`$ si consiglia quindi di usare una procedura iterativa con un array di supporto che raccolga i valori $`r(i))`$, calcolati progressivamente e in modo induttivo, per ogni $`i = 1\dots n`$.

### Modalità di consegna

Si richiede di implementare in un linguaggio a scelta (ad esempio, C, C++, Java) entrambi gli algoritmi descritti sopra, in modo che siano formalmente corretti. Per agevolare la verifica di correttezza da parte del docente sono stati predisposti tre moduli "Virtual Programming Laboratory" (VPL) da utilizzare per caricare il codice degli algoritmi. Una condizione necessaria alla valutazione dell'elaborato è il superamento di tutti i test previsti, per tutti e tre gli algoritmi. Nota: l'esecuzione di un programma lato server attraverso un modulo VPL garantisce uno spazio di memoria di almeno 64KB, giudicato ampiamente sufficiente per risolvere il problema assegnato con qualunque algoritmo fra quelli sopra descritti.

Si richiede di implementare inoltre un programma per la misurazione dei tempi medi di esecuzione dei due algoritmi, al variare della lunghezza $`n`$ della stringa fornita in input.

- La lunghezza $`n`$ della stringa deve essere compresa in un range di valori fra 1000 e 500000, con una distribuzione preferibilmente esponenziale. Ad esempio, $`n`$ potrebbe essere definito da una funzione esponenziale in $`i`$ del tipo $`\left\lceil{A\times B^i}\right\rceil`$, dove $`i=0,1,2,\dots,99`$ e $`A`$ e $`B`$ sono costanti in virgola mobile calcolate opportunamente in modo da avere che $`n=1000`$ quando $`i=0`$ ed $`n=500000`$ quando $`i=99`$.
- La stringa $`s`$ di lunghezza $`n`$ deve essere generata su un alfabeto binario o ternario (es. $`\{'a','b'\}`$, utilizzando uno dei metodi seguenti (a scelta):
  1. le lettere $`s(i)`$ della stringa, per ogni $`i=1,…,n`$, vengono generate in modo pseudo-casuale e indipendentemente una dall'altra;
  2. un parametro $`q`$ compreso fra $`1`$ ed $`n`$ viene generato in modo pseudo-causale; in seguito vengono generate le lettere $`s(i)`$ per ogni $`i=1,…,q`$, seguendo il medoto 1.; infine la parte rimanente di $`s`$ viene generata seguendo la formula $`s(i)=s((i−1) mod q+1)`$ per ogni $`i=q+1,…,n`$;
  3. una variante del metodo 2. viene applcata, con l'eccezione che ad $`s(q)`$ viene assegnata una lettera special (ad esempio, 'c') differente da tutte quelle generate in modo pseudo-casuale;
  4. in aggiunta (e opzionalmente) è possibile misurare i tempi di esecuzione per una costruzione deterministica di $`s`$ che possa fungere da caso pessimo nell'analisi di complessità dell'algoritmo *PeriodNaive*

Opzionalmente, è possibile studiare la distribuzione di probabilità del periodo frazionario minimo per le stringhe generate con i metodi sopra indicati e per un $`n`$ fissato a piacere (es. 100).
 
Per ogni stringa $`s`$ di lunghezza n generata, occorre quindi stimare il tempo di esecuzione per ognuno dei due algoritmi, *PeriodNaive* o *PeriodSmart*. Si richiede una stima del tempo medio di esecuzione al variare della lunghezza $`n`$ della stringa che garantisca un errore relativo massimo pari a 0.001. A tal fine si procede nel modo seguente.

- Per tutte le misurazioni del tempo trascorso è necessario utilizzare un clock di sistema monotono (utilizzare, ad esempio, la procedura `clock_gettime(CLOCK_MONOTONIC, &timeNow)` della libraria `<time.h>` del linguaggio C, oppure il metodo `steady_clock::now()` della libreria `<chrono>` del C++, oppure `System.nanoTime()` del linguaggio Java).
- Il primo passo consiste nello stimare la *risoluzione* del clock di sistema, utilizzando un ciclo while per calcolare l'intervallo minimo di tempo misurabile. A tale scopo è possibile utilizzare uno dei seguenti frammenti di codice in linguaggio C, C++, Java:
```c
// linguaggio C
#include <time.h>
...
long getResolution() {                        // fare attenzione alle divisioni su valori di tipo intero... 
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);
    do {
        clock_gettime(CLOCK_MONOTONIC, &end);
    } while(start.tv_nsec == end.tv_nsec);
    return (end.tv_nsec - start.tv_nsec);
}
```

```c++
#include <stdlib.h>
#include <chrono>
...
using namespace std;
using namespace std::chrono;
...
double getResolution() {
    steady_clock::time_point start = steady_clock::now();
    steady_clock::time_point end;
    do {
        end = steady_clock::now();
    } while (start == end);
    typedef duration<double, seconds::period> duration;
    return duration_cast<secs>(end - start).count();
}
```

```java
import java.util.*;
...
class ... {
    ...
    private static double getResolution() {
       double start = System.nanoTime();
       double end;
       do {
           end = System.nanoTime();
       } while (start == end);
       return end - start;
}
```
- Successivamente, in funzione della risoluzione stimata $`R`$ e dell'errore relativo massimo ammissibile ($`E=0.001`$), si calcola il tempo minimo misurabile
$`T_{min}=R\times \left( \frac 1 E + 1\right)`$
- Infine, si utilizza un ciclo *while* per iterare l'esecuzione dell'algoritmo, misurando il tempo trascorso dall'inizio dell'iterazione, fino a quando tale tempo non risulti superiore a $`T_{min}`$. Il tempo medio di esecuzione per una singola istanza di input sarà quindi dato dal rapporto fra il tempo trascorso e il numero di iterazioni eseguite.
Nel caso si utilizzi il linguaggio di programmazione Java, occorre prestare attenzione a non allocare ripetutamente grandi strutture dati (esempio, array o stringhe) in modo dinamico (ad esempio, con l'istruzione new). Tale pratica potrebbe esaurire in breve tempo la memoria RAM disponibile e attivare il garbage collector, creando picchi nei tempi di esecuzione misurati.
- Opzionalmente, è possibile stimare anche la deviazione standard dei tempi di esecuzione rispetto al tempo medio, in funzione del parametro $`n`$. In tal caso si procederà effettuando un certo numero di misurazioni (ad esempio, un centinaio) sui tempi di esecuzione per una lunghezza $`n`$ fissata, facendo però variare la stringa di lunghezza $`n`$ generata in modo pseudo-casuale. Ogni misurazione sarà effettuata seguendo il metodo sopra descritto, e il risultato di tale misurazione viene memorizzato in un opportuno array. Una volta popolato l'array con le misurazioni, si procederà nel modo classico calcolando media e scarto quadratico medio.


I dati raccolti devono essere presentati e discussi in una relazione in formato PDF da caricare sul server. La valutazione della relazione e del codice sorgente contribuirà in modo significativo al voto finale del progetto di laboratorio. Non è necessario inviare una relazione con molte pagine: qualche decina di pagine è largamente sufficiente a discutere gli aspetti importanti dell'implementazione e dell'analisi dei tempi di esecuzione. *Si consiglia l'uso di grafici comparativi, sia in scale lineari* - $`n`$ vs $`t(n)`$ - *che doppiamente logaritmiche* - $`(log(n))`$ vs $`log(t(n))`$.

**NOTA BENE**: Durante l'elaborazione del progetto, sarà possibile caricare diverse versioni dei programmi sui vari moduli VPL (sostituendo ovviamente i vecchi file). Il progetto viene considerato come consegnato una volta che la [relazione in formato PDF](https://elearning.uniud.it/moodle/mod/assign/view.php?id=69543) viene caricata online. A partire da quel momento il docente potrà prendere visione della relazione e dei codici sorgente forniti.

Per lo svolgimento del progetto, sono ammessi gruppi di **minimo 2 e massimo 4 persone**. Il sito e-learning dell'università non permette la creazione autonoma di gruppi; si consiglia quindi di procedere scegliendo un "rappresentante" per ogni gruppo, che avrà la responsabilità di caricare tutti i file sul sito, inclusa la relazione finale. **È importante che la relazione riporti in chiaro i nomi, i cognomi, le email, e i numeri di matricola di ciascun componente del gruppo.**

Non è fissata una data di consegna vincolante per gli elaborati. Gli elaboratori dovranno però essere consegnati entro la fine dell'anno accademico. Si consiglia inoltre di cercare di consegnare la prima parte del progetto entro l'inizio del secondo semestre, prima di cominciare a lavorare sulla seconda parte del progetto. I progetti consegnati verranno valutati dal docente a intervalli regolari (approssimativamente ogni 2-3 settimane). Qualora fosse necessaria una valutazione in tempi brevi, si prega di contattare il docente per e-mail (gabriele.puppis@uniud.it).



