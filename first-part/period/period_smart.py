#!/bin/python
"""Smart implementation of period function"""

def period_smart(string: str) -> int:
    """Smart implementation of period function"""

    n = len(string)
    computed = [None, 0]                               # bordi computati
    for i in range(1, n):
        j = i
        while j != 0 and string[computed[j]] != string[i]:
            j = computed[j]
        if j == 0:
            computed.append(0)
        else:
            computed.append(computed[j] + 1)
    return n - computed[-1]
            

def main():
    from sys import stdin, stdout
    s = stdin.read()[:-1]
    stdout.write(str(period_smart(s)))


if __name__ == '__main__':
    main()
