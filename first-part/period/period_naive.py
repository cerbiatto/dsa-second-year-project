#!/usr/bin/env python
"""Naive implementation of period function"""

def period_naive(string: str) -> int:
    """Naive implementation of period function"""
    
    n = len(string)
    for p in range(1, n + 1):
        # if prefix/suffix return p
        if all(string[i] == string[p + i] for i in range(n - p)):  
            return p
    return n

def main():
    # To be used for server tests
    from sys import stdin, stdout
    s = stdin.read()[:-1]
    stdout.write(str(period_naive(s)))


if __name__ == '__main__':
    main()
